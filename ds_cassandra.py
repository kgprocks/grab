from datetime import datetime
import json
import sys

from confluent_kafka import Consumer, KafkaError
from cassandra.cluster import Cluster
from cassandra.query import BatchStatement

cluster = Cluster()
session = cluster.connect('grab')


def save_to_cassandra(message):
    insert_ds = session.prepare(
        "INSERT INTO demand_supply (geohash, insertion_time, demand, supply) VALUES (?, ?, ?, ?)")
    batch = BatchStatement()
    message = json.loads(message)

    timestamp = message.pop('timestamp')
    timestamp_object = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')

    for geohash, value in message.items():
        batch.add(insert_ds, (geohash, timestamp_object, int(value['demand']), int(value['supply'])))
    session.execute(batch)


def consume_message(brokers, topic):
    c = Consumer({
        'bootstrap.servers': brokers,
        'group.id': 'ds_cassandra',
        'default.topic.config': {
            'auto.offset.reset': 'smallest'
        }
    })

    c.subscribe([topic])
    try:
        running = True
        while (running):
            msg = c.poll(timeout=1.0)
            print 'working', msg
            if msg:
                if not msg.error():
                    print(msg.value().decode('utf-8'))
                    save_to_cassandra(msg.value().decode('utf-8'))
                elif msg.error().code() != KafkaError._PARTITION_EOF:
                    print(msg.error())
                    running = False
    except KeyboardInterrupt:
        print('Aborted by user')
    finally:
        print('closing the connection.')
        c.close()


if __name__ == "__main__":
    brokers, topic = sys.argv[1:]
    print brokers, topic
    message = consume_message(brokers, topic)
