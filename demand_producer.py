from kafka import KafkaProducer
import random
import time
import math

radius = 10000  # Choose your own radius
radiusInDegrees = radius / 111300.0
r = radiusInDegrees
x0 = 40.84
y0 = -73.87


def random_lat_lng_generator():
    u = float(random.uniform(0.0, 1.0))
    v = float(random.uniform(0.0, 1.0))

    w = r * math.sqrt(u)
    t = 2 * math.pi * v
    x = w * math.cos(t)
    y = w * math.sin(t)

    xLat = str(x + x0)
    yLong = str(y + y0)
    return [xLat, yLong]


data = ['bipul', 'some', 'random', 'number']
producer = KafkaProducer(bootstrap_servers='localhost:9092')

while (1):
    time.sleep(1)
    # random_string = data[random.randint(0, 3)]
    random_string = ','.join([data[random.randint(0, 3)]] + random_lat_lng_generator())
    print(random_string)
    producer.send('demand', random_string)
