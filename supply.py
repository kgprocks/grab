import Geohash
import redis
import sys

from confluent_kafka import Consumer, KafkaError

conn = redis.StrictRedis(host='localhost', port=6379, db=0)


def conversion(lat, lng):
    geohash = Geohash.encode(float(lat), float(lng), precision=6)
    return geohash


# car_id, lat, lng
def process_message(messages):

    parsed_message = list(map(lambda x: x.split(','), messages))
    geohash_dict = {}
    list_of_car_ids = []

    for i in reversed(parsed_message):
        if i[0] not in list_of_car_ids:
            geohash_dict[i[0]] = conversion(i[1], i[2])
            list_of_car_ids.append(i[0])

    result = conn.mget(list_of_car_ids)

    pipe = conn.pipeline()
    for car_id, geohash_id in zip(list_of_car_ids, result):
        if geohash_id is None:
            pipe.set(car_id, geohash_id)
            pipe.incrby(geohash_id + ':supply')

        elif geohash_id != geohash_dict[car_id]:
            pipe.set(car_id, geohash_dict[car_id])
            pipe.incrby(geohash_dict[car_id] + ':supply')
            pipe.incrby(geohash_id + ':supply', -1)

    res = pipe.execute()
    return res


def consume_message(brokers, topic):
    c = Consumer({
        'bootstrap.servers': brokers,
        'group.id': 'ds_group',
        'default.topic.config': {
            'auto.offset.reset': 'smallest'
        }
    })
    c.subscribe([topic])
    # c.subscribe(['supply_0', 'supply_1', 'supply_2', 'supply_3', 'supply_4'])

    try:
        running = True
        messages = []
        while (running):
            msg = c.poll()
            if not msg.error():
                messages.append(msg.value().decode('utf-8'))
                if len(messages) == 10:
                    print(messages)
                    process_message(messages)
                    messages = []
            elif msg.error().code() != KafkaError._PARTITION_EOF:
                print(msg.error())
                running = False
    except KeyboardInterrupt:
        print('Aborted by user')
    finally:
        print('closing the connection.')
        c.close()


if __name__ == "__main__":
    brokers, topic = sys.argv[1:]
    message = consume_message(brokers, topic)
