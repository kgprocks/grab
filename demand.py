from kafka import KafkaProducer
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

import datetime
import Geohash
import json
import redis
import sys

brokers, topic = sys.argv[1:]


def conversion(text):
    user_id, lat, lng = text.split(',')
    geohash = Geohash.encode(float(lat), float(lng), precision=6)
    return (geohash, [user_id])


def windowed_function(v1, v2):
    return len(set(v1 + v2))


def list_conversion(values):
    if isinstance(values, list):
        return 1
    return values


def saveToRedis(iter):
    # TODO: Use connection pool for better performance.
    conn = redis.StrictRedis(host='localhost', port=6379, db=0)
    pipe = conn.pipeline()
    geohash_list = []
    ds_dict = {}
    for record in iter:
        pipe.setex(record[0] + ':demand', record[1], 20)
        pipe.get(record[0] + ':supply')
        geohash_list.append(record[0])
        ds_dict[record[0]] = {'demand': record[1]}

    results = pipe.execute()

    for i in range(1, len(results), 2):
        geohash = geohash_list[i / 2]
        if results[i]:
            ds_dict[geohash]['supply'] = results[i]
        else:
            ds_dict[geohash]['supply'] = 0
    ds_dict['timestamp'] = str(datetime.datetime.now())
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    producer.send('demand_supply', json.dumps(ds_dict))
    producer.flush()


def functionToCreateContext():
    sc = SparkContext("local[2]", "SparkDSCalculator")

    sc.setLogLevel("WARN")
    ssc = StreamingContext(sc, 10)
    kvs = KafkaUtils.createDirectStream(ssc, [topic], {"metadata.broker.list": brokers})

    message = kvs.map(lambda x: x[1])
    message.map(conversion).reduceByKeyAndWindow(
        windowed_function, 20, 10).mapValues(list_conversion).foreachRDD(lambda rdd: rdd.foreachPartition(saveToRedis))

    return ssc


if __name__ == "__main__":
    brokers, topic = sys.argv[1:]
    ssc = StreamingContext.getOrCreate('/home/ec2-user/checkpoint1', lambda: functionToCreateContext())

    ssc.start()
    ssc.awaitTermination()
